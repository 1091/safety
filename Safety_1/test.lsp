(define (getExplodedText fileName)
	(if (file? fileName) (explode (read-file fileName)) null)	
 )

	
(define (unpackStr str)
	(apply append (map (lambda (elem) (unpack "c" elem)) str))
 )

(define (packStr str)
	(apply append (map (lambda (elem) (pack "c" elem)) str))
 )


(define (convertKey key)
	(apply list (map read-expr (explode key)))
 )

	
(define (makeLongKey text bigKey key )
	(cond
	 ( (> (length text) (length bigKey)) (makeLongKey text (append bigKey key) key) )
	 ( true bigKey )
	 )
 )
		

(define (gronsfeld explodedText key cryptoFlag)
 	(cond
	  ( (= cryptoFlag 1)	(map + (unpackStr explodedText)  (makeLongKey explodedText key key)  ) )
 	  ( (= cryptoFlag 0)    (map - (unpackStr explodedText)  (makeLongKey explodedText key key)  ) )  
	)
 )


(define  (Crypto fileName key)
	(if (null? (getExplodedText fileName))  (exit 1) )
	;(if (not(list? key)) (exit 2))
	(write-file "Crypto.txt" (packStr (gronsfeld (getExplodedText fileName) (convertKey  key) 1))) 
	 	
 )

(define  (Uncrypto fileName key)
	(if (null? (getExplodedText fileName))  (exit 1) )
	;(if (not(list? key)) (exit 2))
	(write-file "Uncrypto.txt"  (packStr (gronsfeld (getExplodedText fileName) (convertKey key) 0))) 
	
 )
