class cracker
{
	def readFileToList(file:String) = {	
		io.Source.fromFile(file).getLines.toList 
	}
	
	def hexToInt(hexStr:String) = { 
		Integer.parseInt(hexStr, 16) 
	} 
	
	def hexStrToIntArr(hexStr:String) = {
		hexStr.sliding(2, 2).toArray.map(hexToInt(_))
	}
	
	def recMysterySearch(arr : List[Array[Int]], ciphIndex : Int, keyIndex : Int ) : Int = {
		
		if(ciphIndex >= arr.length) return -1
		val xored : List[Int]  = for(a <- arr; if (a.length>keyIndex && arr(ciphIndex).length>keyIndex) ) yield 
						a(keyIndex)^arr(ciphIndex)(keyIndex)
		 
		if( xored.count( x=> (x>=55 && x<=90) || (x>=97 && x<=122)) >= 0.5*xored.length  )
			ciphIndex
		else
			recMysterySearch(arr, ciphIndex+1, keyIndex)
	}
}

object main
{
	def main(args:Array[String]){
		val crack = new cracker() 
		val cipherList = crack.readFileToList("ciphers")
		val devidedCipherList = for( str <- cipherList) yield crack.hexStrToIntArr(str)
		val keyIndexes = for(i<-0 until devidedCipherList(10).length) yield crack.recMysterySearch(devidedCipherList, 0, i)
		val key = for( i<-0 until keyIndexes.length) yield if(keyIndexes(i) == -1) -1 else devidedCipherList(keyIndexes(i))(i)^32
		val result = for(i<-0 until devidedCipherList(10).length)  yield if(key(i) == -1) '?' else (devidedCipherList(10)(i)^key(i)).toChar  
		println(result.mkString)
		
	}
}
