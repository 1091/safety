TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
LIBS += -lcrypto

QMAKE_CXXFLAGS += -std=c++11
SOURCES += main.cpp \
    chappybirthdayattack.cpp \
    cshadevider.cpp

HEADERS += \
    chappybirthdayattack.h \
    cshadevider.h

