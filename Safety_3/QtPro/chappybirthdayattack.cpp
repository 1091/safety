#include "chappybirthdayattack.h"
#include <ctime>
#include <utility>
cHappyBirthdayAttack::cHappyBirthdayAttack(int start, int count)
{
    shaDevider = new cShaDevider(start, count);
    srand(time(0));
}

char cHappyBirthdayAttack::getRandomCh()
{
     return alphanum[rand() % alphanumLength];
}

std::string cHappyBirthdayAttack::getRandomStr()
{
    int strLength = 32 + rand()%96;
    std::string resultStr="";

    for(int i=0; i<strLength; ++i)
        resultStr+=getRandomCh();

    return resultStr;
}

void cHappyBirthdayAttack::run()
{

    std::string msg;
    std::string sha;
    bool insRes=false;
    long int counter = 0;
    do{
        counter++;
        msg = getRandomStr();
        sha = shaDevider->getPartitionSHA256(msg);
        auto pair = std::make_pair(sha, msg);
        insRes = msg_sha_MAP.insert(pair).second;
    }while(insRes);

    std::cout<<"Collision was found!"<<std::endl;
    std::cout<<msg<<std::endl;
    std::cout<<sha<<std::endl;
    std::cout<<std::endl;

    auto find = msg_sha_MAP.find(sha);
    std::cout<<find->first<<std::endl;
    std::cout<<find->second<<std::endl;
    std::cout<<"counter=" << counter << std::endl;
}
