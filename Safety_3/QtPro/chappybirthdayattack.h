#ifndef CHAPPYBIRTHDAYATTACK_H
#define CHAPPYBIRTHDAYATTACK_H
#include <map>
#include "cshadevider.h"
#include <iostream>
#include <string>
#include <cstdlib>
class cHappyBirthdayAttack
{

public:

    cHappyBirthdayAttack(int start, int count);
    void run();
    std::string getRandomStr();
private:
    cShaDevider *shaDevider;
    std::map <std::string, std::string> msg_sha_MAP;

    char getRandomCh();


    const std::string alphanum = "0123456789 !@#$%^&* ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz";

    const int alphanumLength = alphanum.length() - 1;

};

#endif // CHAPPYBIRTHDAYATTACK_H
