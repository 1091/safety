#include "cshadevider.h"
#include <stdexcept>
cShaDevider::cShaDevider(int start, int count):startBit(start), countBit(count)
{
    if(start>sha256_bit_maxSize ||
            start < 0 ||
            count+start > sha256_bit_maxSize ||
            count < 0
      )
        throw std::runtime_error("cShaDevider: Bad params!");
}
std::vector <u_char> cShaDevider::getSHA256(std::string inpStr)
{
    unsigned char digest[SHA256_DIGEST_LENGTH];

    SHA256((unsigned char*)inpStr.c_str(), inpStr.length(), (unsigned char*)digest);

    std::vector<u_char> result(digest, digest+sizeof(digest)/sizeof(u_char));
    return result;
}

std::string cShaDevider::getPartitionSHA256(std::string inpStr)
{
    std::vector <u_char> sha256 = getSHA256(inpStr);

    int start_byteShift = startBit / 8;
    int start_bitShift = startBit % 8;


    u_char firstByte = sha256[start_byteShift];
    u_char firsByte_bitMulter = (255>>start_bitShift); //ok bool math
    /*slice start_bitShift==5 bits
     *10101101 (firstByte)
     *         &
     *00000111  (255>>5)
     *--------
     *00000101
     */
    u_char res_firstByte = firstByte & firsByte_bitMulter;

    u_char res_lastByte;
    int last_byteShift=start_byteShift;
    //if(countBit > 8){
        last_byteShift = start_byteShift + countBit/8  + ((countBit % 8)>0 ? 1 : 0);
        int last_bitShift = (startBit + countBit) % 8;

        u_char lastByte = sha256[last_byteShift];
        u_char lastByte_bitMulter;//

        if(last_bitShift==0) //feature case when last byte becomes 0; Trust me i'm an engineer
            lastByte_bitMulter=255;
        else //ok bool math
            lastByte_bitMulter= ~(255>>last_bitShift);

        /*slice last_bitShift==5 bits
         *10111011 (lastByte)
         *         &
         *11111000 ~(255>>5)
         *---------
         *10111000
         */
        res_lastByte = lastByte & lastByte_bitMulter;
    //}
    std::string result;
    char buf[2];
    sprintf(buf,"%02x",res_firstByte);
    result+=buf;
    for(int i = start_byteShift+1; i<last_byteShift-1; i++){
        sprintf(buf,"%02x",sha256[i]);
        result+=buf;
    }
    //if(countBit>8){
        sprintf(buf,"%02x",res_lastByte);
        result+=buf;
    //}
    return result;
}
