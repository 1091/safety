#ifndef SHADEVIDER_H
#define SHADEVIDER_H
#include <vector>
#include <string>
#include <openssl/sha.h>

class cShaDevider
{
public:
    cShaDevider(int start, int count);
    std::string getPartitionSHA256(std::string);

private:
    const int sha256_bit_maxSize = SHA256_DIGEST_LENGTH * 8;
    std::vector <u_char> getSHA256(std::string);
    int startBit;
    int countBit;
};

#endif // SHADEVIDER_H
