#include <iostream>
#include <stdio.h>
#include <string.h>
#include <vector>
#include <openssl/sha.h>
#include "chappybirthdayattack.h"

#include <chrono>
using namespace std;


int main()
{
    typedef std::chrono::duration <int> seconds;
    using namespace std::chrono;

    cHappyBirthdayAttack happy(12,30);
    auto begin = high_resolution_clock::now();
    happy.run();

    auto end = high_resolution_clock::now();
    std::cout << std::endl;
    std::cout << duration_cast <milliseconds>( end - begin ).count() << std::endl;

    return 0;
}
