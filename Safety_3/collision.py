import string
import random
import hashlib

def str_generator():
	chars=string.ascii_letters + string.digits
	length = random.randint(16, 64)
	return ''.join(random.choice(chars) for _ in range(length))

#get hex digest of $string
def hashing(string):
	return hashlib.sha256(string).hexdigest()

#get part of hash 
def hashDevider(hashStr, startShift, size):
	if(len(hashStr)!=32*2):
		raise Exception("hashDevider: wrong hash length")
	if(startShift<0 or size<0 or startShift+size>32*2):
		raise Exception("hashDevider: wrong startShift/size params")

	hashListStr = [hashStr[i:i+2] for i in range(0, len(hashStr), 2)]
	hashList = list(map(lambda elem: int(elem, 16), hashListStr))
	start_byteShift = startShift/8
	start_bitShift = startShift%8

	last_bitShift = (startShift + size  )%8
	last_byteShift = (startShift + size )/8 + (1 if last_bitShift>0 else 0) 
	
	resList = []
	
	if(start_byteShift == last_byteShift):	
		return '{:02x}'.format ((hashList[last_byteShift] & ~(255>>last_bitShift)))
	
	first_byte = hashList[start_byteShift] & (255>>start_bitShift)
	last_byte = hashList[last_byteShift] &  ~(255>>last_bitShift)

	resList.append(first_byte)
	for i in range(start_byteShift+1, last_byteShift-1):
		resList.append(hashList[i])
	resList.append(last_byte)
	
	resStr = ""
	for x in range(0, len(resList)):
		resStr += '{:02x}'.format( resList[x] )
	return resStr
	

found = False
messages = {}
newStr = ""
hStr = ""
start = 1
size = 40

counter = 0
while not found:
	counter+=1
	newStr = str_generator()
	hStr =	hashDevider(hashing(newStr),start,size)
	try:
		found = True
		#print(hStr)
		x = messages[hStr]
	except Exception:
		messages.update({hStr : newStr})
		found = False


print("Collision was found")
print(newStr)
print( hashDevider(hashing(newStr),start,size) )

print(messages[hStr])
print ( hashDevider(hashing(messages[hStr]),start,size) )

print('counter=', counter)
