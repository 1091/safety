#include <gmp.h>
#include <gmpxx.h>
#include <iostream>
#include <map>
#include <chrono>
void meet(long B, mpz_class &mod, mpz_class &base, mpz_class &num)
{
	std::map <std::string, long> leftPart;
	mpz_class powBase;
	mpz_powm(powBase.get_mpz_t(), base.get_mpz_t(), mpz_class(B).get_mpz_t(), mod.get_mpz_t());
	
	for(long x1 = 1; x1 < B; ++x1){
		mpz_class mpz_x1(-x1), mpowRes, modmulRes;
		mpz_powm(mpowRes.get_mpz_t(), base.get_mpz_t(), mpz_x1.get_mpz_t(), mod.get_mpz_t());

		modmulRes=(num*mpowRes)%mod;
		//std::cout << x1 << ":" << modmulRes << "\n";
		leftPart.insert(make_pair(modmulRes.get_str(), x1));
	}
	std::cout << "Map building finished\n";	
	for(long x0 = 1; x0 < B; ++x0){
		mpz_class mpowBase, mpz_x0(x0);
		mpz_powm(mpowBase.get_mpz_t(), powBase.get_mpz_t(), mpz_x0.get_mpz_t(), mod.get_mpz_t());
	
		auto find = leftPart.find(mpowBase.get_str());
		if(find != leftPart.end()){
			std::cout << "FIND!\n";
			std::cout << "x0=" << x0 << "\n";
			std::cout << "x1=" << find->second << "\n";
			break;		
		}
	}
} 	

 
int main(void)
{
	long B = 1048576;//2^20
	mpz_class mod("13407807929942597099574024998205846127479365820592393377723561443721764030073546976801874298166903427690031858186486050853753882811946569946433649006084171");
	mpz_class base("11717829880366207009516117596335367088558084999998952205599979459063929499736583746670572176471460312928594829675428279466566527115212748467589894601965568");
	mpz_class num("3239475104050450443565264378728065788649097520952449527834792452971981976143292558073856937958553180532878928001494706097394108577585732452307673444020333");
	
	auto begin = std::chrono::high_resolution_clock::now();
	meet(B,mod, base, num);
	auto end =   std::chrono::high_resolution_clock::now();
	std::cout<<"time="<< std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << "\n";
	return 0;
}
