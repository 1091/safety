#include <string>
#include <iostream>

#include <NTL/ZZ.h>
#include <NTL/tools.h>

#include <NTL/ZZ_p.h>
#include <map>
#include <sstream>

#include <chrono>
using namespace NTL;
using namespace std;
NTL_CLIENT


void meet(long B, ZZ &p, ZZ&g, ZZ &h){
    ZZ_p::init(p);

    ZZ_p f, hp, gp, gx, gb, gbx;

    conv(hp, h);
    conv(gp, g);
    conv(gb,g);
    gb = power(gb, B);

    map<string, long> mapa;
    for(long x1 = 1; x1 < B; ++x1){
        ostringstream ss;
        gx = power(gp, -x1);

        f=hp*gx;

        ss << f;

        mapa.insert(make_pair(ss.str(), x1));
    }

    cout<< "Table construction finished\n";

    for(long x0 = 1; x0 < B; ++x0){
        ostringstream ss;
        gbx = power(gb, x0);

        ss << gbx;

        auto find = mapa.find(ss.str());

        if(find != mapa.end()){
            cout << "I'M' FIND IT!!!\n\n";
            cout << ss.str() << "\n\n";
            cout << find->first << "\n\n";
            cout << "x1=" << find->second << "\n\n";
            cout << "x0=" << x0 << "\n\n";
            break;
        }
    }
    cout << "END\n";
}


int main()
{
    ZZ p, g, h[4];
    long B = 1048576;//2^20
    conv(p, "134078079299425970995740249982058461274793658205923933777"
            "23561443721764030073546976801874298166903427690031858186486"
            "050853753882811946569946433649006084171");

    conv(g, "117178298803662070095161175963353670885580849999989522055"
            "99979459063929499736583746670572176471460312928594829675428"
            "279466566527115212748467589894601965568");

    ZZ hh;
    conv(hh, "32394751040504504435652643787280657886490975209524495278347924"
         "52971981976143292558073856937958553180532878928001494706097394108"
         "577585732452307673444020333");

    conv(h[0], 22L);
    conv(h[1], 23L);
    conv(h[2], 2L);
    h[2] = power(h[2],100L);
    h[2]+=11;

    conv(h[3], 10l);
    h[3] = power(h[3], 100L);
    h[3]+=11;

    using namespace std::chrono;
    auto begin = high_resolution_clock::now();
    meet(B, p, g, hh);
    auto end = high_resolution_clock::now();
    std::cout << std::endl;
    std::cout <<"time=" << duration_cast <milliseconds>( end - begin ).count() << std::endl;
}


