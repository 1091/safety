#include <iostream>
#include <gmpxx.h>
using namespace std;

int main()
{

    mpz_class N("415322946759158466310352770887385936902012232738560119"
              "816479766730221458887063558269310090941395211596173562"
              "712343609754540971799206357500060590437775050244070653"
              "829921526853693548663404123456292103778862001415037509"
              "2399385273967413490630080326376508438534969408415057");

    mpz_class sqrtN, A;
    mpz_sqrt(sqrtN.get_mpz_t(), N.get_mpz_t());
    A = sqrtN+1;
    mpz_class x(0);
    const int primeTestCount = 25;
    while(1){
        mpz_class tmp=A*A - N;
        mpz_sqrt(x.get_mpz_t(), tmp.get_mpz_t());
        mpz_class p = A-x;
        mpz_class q = A+x;

        if( p*q == N )
        {
            if( mpz_probab_prime_p(p.get_mpz_t(), primeTestCount) > 0 && mpz_probab_prime_p(q.get_mpz_t(), primeTestCount) >0){
                cout << "p="<<p<<"\nq="<<q<<"\np*q=" <<p*q << endl;
                break;
            }
        }
        A++;
    }

    return 0;
}


