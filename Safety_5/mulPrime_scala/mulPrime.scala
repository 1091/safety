object mulPrime
{
	//pure copy-paste magic
	def sqrt(number : BigInt) = {
		def next(n : BigInt, i : BigInt) : BigInt = (n + i/n) >> 1
		
		val one = BigInt(1)
		
		var n = one
		var n1 = next(n, number)
		   
		while ((n1 - n).abs > one) {
		  n = n1
		  n1 = next(n, number)
		}
			
		while (n1 * n1 > number) {
		  n1 -= one
		}
			
		n1
	}

	var N:BigInt = BigInt(1)
	val primeTestCount = 10

	def recSearch(A:BigInt):Vector[BigInt] = {
		if(A>N/2) Vector() else {
			val x = sqrt(A.pow(2) - N)
			val p = A-x
			val q = A+x
			if( p*q == N && p.isProbablePrime(primeTestCount) && q.isProbablePrime(primeTestCount)  )
				Vector(p,q)
			else
				recSearch(A+1) 
		}

	}
	def Search() = {
		recSearch(sqrt(N)+1)
	}
	
	def apply() = {
		N = BigInt("4153229467591584663103527734652081084542966865874571772034479616156220955868803785073923483203747386484721760437261620676499602008407539610606923795797325387364536407630903324001718182662407233448655712045320134496575622475655892957009417550602399221412298302821320879")
		Search()
	}
}

object main extends App
{
	println(mulPrime())
}
